#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/petey/mappr/src/joy_translate/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/petey/mappr/src/joy_translate/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/petey/mappr/src/joy_translate/build/devel/lib:/home/petey/mappr/src/joy_translate/build/devel/lib/x86_64-linux-gnu:/home/petey/mappr/install/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/home/petey/mappr/install/lib:/opt/ros/indigo/lib"
export PATH="/home/petey/mappr/src/joy_translate/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/petey/mappr/src/joy_translate/build/devel/lib/pkgconfig:/home/petey/mappr/src/joy_translate/build/devel/lib/x86_64-linux-gnu/pkgconfig:/home/petey/mappr/install/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/home/petey/mappr/install/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PYTHONPATH="/home/petey/mappr/src/joy_translate/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/petey/mappr/src/joy_translate/build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/petey/mappr/src/joy_translate:$ROS_PACKAGE_PATH"