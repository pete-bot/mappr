# CMake generated Testfile for 
# Source directory: /home/petey/mappr/src/rosserial/rosserial_client
# Build directory: /home/petey/mappr/src/rosserial/rosserial_client
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_rosserial_client_gtest_float64_test "/home/petey/mappr/src/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/petey/mappr/src/test_results/rosserial_client/gtest-float64_test.xml" "--return-code" "/home/petey/mappr/src/devel/lib/rosserial_client/float64_test --gtest_output=xml:/home/petey/mappr/src/test_results/rosserial_client/gtest-float64_test.xml")
