// program to allow easy control of a robot using an xbox controller


#include <ros/ros.h>

#include <iostream>
#include <sensor_msgs/Joy.h>    // messages from joy - contain xboxdrv content
#include <geometry_msgs/Twist.h>  // messages to output twist message

// need to subscribe to joy topic
// need to publish special twist message - this will be subscribed to by the 

class JoyDriver
{
    ros::NodeHandle nh_;
    ros::Subscriber joy_sub_;   
    ros::Publisher joy_pub_;
    ros::Publisher joy_cam_pub_; 

    public:
        JoyDriver(const ros::NodeHandle& n);

    private:
        void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);
};

JoyDriver::JoyDriver(const ros::NodeHandle& n)
    {
        nh_ = n;
        joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &JoyDriver::joyCallback, this);
        joy_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 10);
        joy_cam_pub_ = nh_.advertise<geometry_msgs::Twist>("camera_cmd_vel", 10);
    }

void  JoyDriver::joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
{
    // movement translation
    geometry_msgs::Twist vel_msg;
    vel_msg.linear.x = 4*joy_msg->axes[1];
    vel_msg.angular.z = 4*joy_msg->axes[0];
    joy_pub_.publish(vel_msg);

    // camera translation
    geometry_msgs::Twist cam_vel_msg;
    cam_vel_msg.linear.x = 4*joy_msg->axes[3];
    cam_vel_msg.angular.z = 4*joy_msg->axes[2];
    joy_cam_pub_.publish(cam_vel_msg);
} 

int main(int argc, char** argv) {

    std::cout << "Starting Joy->base_drive_message translation service." << std::endl;

    ros::init(argc, argv, "joy_control");
    ros::NodeHandle n;
    JoyDriver jd(n);

    ros::Rate loop_rate(10);

    while (n.ok()) {
        ros::spinOnce();
    }
}
