/*
 * motor control system w/ subsriber to ROS topic: joy_twist
 * use geometry_msgs::Twist to control motor behaviour
 */

#define DEADZONE 1

// MOTOR OBJECTS
#define E1 6     //M2 Speed Control       ; LEFT MOTOR
#define M1 7     //M2 Direction Control   ; 
#define M2 4     //M1 Direction Control   ; RIGHT MOTOR (boolean)
#define E2 5     //M1 Speed Control       ; RIGHT MOTOR (range 2^8)

#define PAN_PIN 9
#define TILT_PIN 8


#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <Servo.h> 
#include <SoftwareServo.h> 


void cmd_vel_CallBack( const geometry_msgs::Twist& joy_twist);
void camera_cmd_vel_CallBack( const geometry_msgs::Twist& joy_twist);

// ROS OBJECTS
ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Twist> cmd_vel_sub("cmd_vel", &cmd_vel_CallBack );
ros::Subscriber<geometry_msgs::Twist> camera_cmd_vel_sub("camera_cmd_vel", &camera_cmd_vel_CallBack );



// SERVO OBJECTS
Servo tilt;     // pin 8 
Servo pan;      // pin 9

// variable to store the servo position 
int posPan = 0;    
int posTilt = 0;



// ROS callback function
void cmd_vel_CallBack( const geometry_msgs::Twist& cmd_vel){

    // calculate correct values for motor controls
    float leftSpeedRaw  = cmd_vel.linear.x + cmd_vel.angular.z;
    float rightSpeedRaw = cmd_vel.linear.x - cmd_vel.angular.z;

    Serial.print("leftSpeedraw: ");
    Serial.println(leftSpeedRaw);

    Serial.print("rightSpeedraw: ");
    Serial.println(rightSpeedRaw);
    
    //float angularRaw = cmd_vel.angular.z;
    //angularRaw = map(angularRaw, -4, 4, -255, 255);


    // TODO
    // improve the max and min value calculation here
    // map raw values.
    float leftMotorScaled  = map(leftSpeedRaw,  -8, 8, -255, 255);
    float rightMotorScaled = map(rightSpeedRaw, -8, 8, -255, 255);

    //apply the results to appropriate uC PWM outputs for the LEFT motor:
    if(abs(leftMotorScaled) > DEADZONE){
      
      if (leftMotorScaled > 0){
        analogWrite(E1,abs(leftMotorScaled));
        digitalWrite(M1,HIGH);
              
      } else {
        analogWrite(E1,abs(leftMotorScaled));  
        digitalWrite(M1,LOW);

      }
   
    // stop the motors
    } else {
      analogWrite(E1,0);
      digitalWrite(M1,HIGH);
    } 


    //apply the results to appropriate uC PWM outputs for the RIGHT motor:  
    if(abs(rightMotorScaled) > DEADZONE){
      
      if (rightMotorScaled > 0){
        analogWrite(E2,abs(rightMotorScaled));
        digitalWrite(M2,HIGH);
                
      // reverse the right motor
      } else {
        analogWrite(E2,abs(rightMotorScaled));
        digitalWrite(M2,LOW);
      }
   
    // stop the motors
    } else {
      analogWrite(E2,0);
      digitalWrite(M2,HIGH);
    } 
}







// ROS callback function
void camera_cmd_vel_CallBack( const geometry_msgs::Twist& camera_cmd_vel){

    Serial.println("CAM CALLBACK.");
    // TILT SET
    tilt.write(tilt.read() - camera_cmd_vel.linear.x );  
    
     // PAN SET
    pan.write(pan.read() + camera_cmd_vel.angular.z );
  

    SoftwareServo::refresh();

}








void setup()
{

  
  //Serial.begin(115200);
  
  // ROS INIT 
  nh.initNode();
  nh.subscribe(cmd_vel_sub);
  nh.subscribe(camera_cmd_vel_sub);
  
  pinMode(E1, OUTPUT);  
  pinMode(M1, OUTPUT);  
  pinMode(E2, OUTPUT);  
  pinMode(M2, OUTPUT);  
  
  digitalWrite(E1,LOW);   
  digitalWrite(E2,LOW); 


  tilt.attach(TILT_PIN);  // attaches the servo on pin 9 to the servo object 
  pan.attach(PAN_PIN);  // attaches the servo on pin 9 to the servo object 

  tilt.write(90);
  pan.write(90);

  
  
  
  
}

void loop()
{
  nh.spinOnce();
  delay(1);
}
