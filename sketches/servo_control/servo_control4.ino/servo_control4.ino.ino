
#include <Servo.h> 
#include <SoftwareServo.h> 


Servo tilt;  // create servo object to control a servo 
Servo pan;

 
int posPan = 0;    // variable to store the servo position 
int posTilt = 0;

void setup() 
{ 
  tilt.attach(9);  // attaches the servo on pin 9 to the servo object 
  pan.attach(10);  // attaches the servo on pin 9 to the servo object 

  tilt.write(85);
  
  Serial.begin(9600);

} 
 
void loop() 
{ 
  Serial.print("Servo1 pos: ");
  Serial.println(tilt.read());

  
  Serial.print("Servo2 pos: ");
  Serial.println(pan.read());
/*
  for(posPan = 0; posPan <= 180; posPan += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    tilt.write(posPan);              // tell servo to go to position in variable 'pos' 
    
    delay(15);
    SoftwareServo::refresh();
  } 

  
  for(posPan = 180; posPan <= 0; posPan -= 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    tilt.write(posPan);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
    SoftwareServo::refresh();

  }



  for(posTilt = 0; posTilt <= 180; posTilt += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    pan.write(posTilt);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
    SoftwareServo::refresh();
  } 

  for(posTilt = 180; posTilt <= 0; posTilt -= 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    pan.write(posTilt);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
    SoftwareServo::refresh();
  }




  for(posTilt = 0; posTilt <= 180; posTilt += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    pan.write(posTilt);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }

  
  for(posTilt = 180; posTilt <= 0; posTilt -= 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    pan.write(posTilt);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  
  
  

*/ 
  SoftwareServo::refresh();
} 

