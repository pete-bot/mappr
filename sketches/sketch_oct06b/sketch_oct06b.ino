/*
 * motor control system w/ subsriber to ROS topic: joy_twist
 * use geometry_msgs::Twist to control motor behaviour
 */


#define ROBOT_WIDTH 0.185

// MOTOR OBJECTS
#define E1 = 5;     //M1 Speed Control
#define E2 = 6;     //M2 Speed Control
#define M1 = 4;     //M1 Direction Control
#define M2 = 7;     //M1 Direction Control



#include <ros.h>
#include <geometry_msgs/Twist.h>

void joyCallBack( const geometry_msgs::Twist& joy_twist);

// ROS OBJECTS
ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Twist> joy_sub("joy_twist", &joyCallBack );

// ROS callback function
void joyCallBack( const geometry_msgs::Twist& cmd_vel){


    float left_speed_out = cmd_vel.linear.x - cmd_vel.angular.z*ROBOT_WIDTH/2;
    float right_speed_out = cmd_vel.linear.x + cmd_vel.angular.z*ROBOT_WIDTH/2;

    Serial.print("leftSpeed: ");
    Serial.print(left_speed_out);
    Serial.print("rightSpeed: ");
    Serial.print(right_speed_out);
    Serial.println();
  
}



void setup()
{
  nh.initNode();
  nh.subscribe(joy_sub);
}

void loop()
{
  nh.spinOnce();
  delay(1);
}
