#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/petey/mappr/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/petey/mappr/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/petey/mappr/devel/lib:/home/petey/mappr/devel/lib/x86_64-linux-gnu:/home/petey/mappr/install/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/home/petey/mappr/install/lib:/opt/ros/indigo/lib"
export PATH="/home/petey/mappr/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/petey/mappr/devel/lib/pkgconfig:/home/petey/mappr/devel/lib/x86_64-linux-gnu/pkgconfig:/home/petey/mappr/install/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/home/petey/mappr/install/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PWD="/home/petey/mappr/build"
export PYTHONPATH="/home/petey/mappr/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/petey/mappr/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/petey/mappr/src:$ROS_PACKAGE_PATH"