# CMake generated Testfile for 
# Source directory: /home/petey/mappr/src
# Build directory: /home/petey/mappr/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(rosserial/rosserial)
SUBDIRS(rosserial/rosserial_arduino)
SUBDIRS(rosserial/rosserial_client)
SUBDIRS(rosserial/rosserial_msgs)
SUBDIRS(rosserial/rosserial_python)
SUBDIRS(rosserial/rosserial_xbee)
SUBDIRS(joy_translate)
SUBDIRS(rplidar_ros)
SUBDIRS(rosserial/rosserial_server)
SUBDIRS(robot_setup_tf)
SUBDIRS(rosserial/rosserial_embeddedlinux)
SUBDIRS(rosserial/rosserial_windows)
SUBDIRS(mappr_2dnav)
