set(_CATKIN_CURRENT_PACKAGE "joy_translate")
set(joy_translate_MAINTAINER "petey <petey@todo.todo>")
set(joy_translate_DEPRECATED "")
set(joy_translate_VERSION "0.0.0")
set(joy_translate_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(joy_translate_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(joy_translate_BUILDTOOL_DEPENDS "catkin")